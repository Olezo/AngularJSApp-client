var todoListApp = angular.module('todoListApp', ['ngRoute']);

todoListApp.config(function ($routeProvider) {
    $routeProvider
        .when('/tasks', {
            controller: 'TasksController',
            templateUrl: 'views/all_tasks.html'
        })
        .when('/add', {
            controller: 'AddTaskController',
            templateUrl: 'views/add_task.html'
        })
        .when('/preview:taskId', {
            controller: 'PreviewTaskController',
            templateUrl: 'views/preview_task.html'
        })
        .otherwise({redirectTo: '/tasks'});

});

todoListApp.factory('todoListFactory', function ($http) {
    var tasks = [];

    loadTasks();

    var factory = {};
    factory.getTasks = function () {
        console.log("Getting data from factory.getTasks()");
        return tasks;
    };
    factory.addTask = function (taskName, taskDescription, isDone) {
        if (taskName != undefined && taskName.length > 5) {
            var newId = tasks.length + 1;
            var currentDate = new Date();
            var newTask = {
                id: newId,
                name: taskName,
                description: taskDescription,
                date: currentDate,
                done: isDone
            };
            tasks.push(newTask);
            console.log("Task saved", newTask);
            return true;
        }
        return false;
    };
    factory.getTask = function (id) {
        for (var i = tasks.length - 1; i >= 0; i--) {
            if (tasks[i].id == id) {
                return tasks[i];
            }
        }
    };
    factory.removeTask = function (id) {
        for (var i = tasks.length - 1; i >= 0; i--) {
            if (tasks[i].id == id) {
                tasks.splice(i, 1);
                break;
            }
        }
    };
    function loadTasks() {
        /* tasks = [
         {id: 0, name: "Angular JS", description: 'Simple name 23', date: 1345546879874, done: true},
         {id: 1, name: "Java", description: 'Simple name 12', date: 1834687875574, done: true},
         {id: 2, name: "JavaScript", description: 'Simple name 23', date: 157346879874, done: false},
         {id: 3, name: "Samsung", description: 'Simple name 23', date: 12346879874, done: true},
         {id: 4, name: "Chicken", description: 'Simple name 23', date: 178346879874, done: false},
         {id: 5, name: "Water", description: 'Simple name 23', date: 1788346879874, done: false},
         {id: 6, name: "Potter", description: 'Simple name 23', date: 1273468279874, done: true}
         ];*/

        // Simple GET request example:
        $http({
            method: 'GET',
            url: 'http://localhost:7777/tasks'
        }).then(function successCallback(response) {
            console.log("response", response);
            for (var i = 0; i < response.data.length; i++) {
                var task = {
                    id: response.data[i].id,
                    name: response.data[i].name,
                    description: response.data[i].description,
                    date: response.data[i].date,
                    done: response.data[i].done
                };
                tasks.push(task);
            }

        }, function errorCallback(response) {
            console.log("response error", response)
        });
    }

    factory.uploadData = function () {
        var url = 'http://localhost:7777/tasks';
        $http.post(url, tasks)
            .then(
                function (response) {
                    console.log('success', response);
                    alert('saved');
                },
                function (response) {
                    console.log('error', response);
                    alert('error');
                }
            );

    };

    return factory;
});