todoListApp.controller('AddTaskController', function ($scope, todoListFactory) {
    console.log("AddTaskController creating..");
    $scope.alert = {};
    $scope.showAlert = false;
    $scope.hasError = false;

    $scope.addTask = function () {
        console.log("Adding task..");
        if (todoListFactory.addTask($scope.newTask.name, $scope.newTask.description, $scope.newTask.done)) {
            clearFields();
            $scope.alert = {header: "Success", message: "Item was successfully added"};
            $scope.hasError = false;
        } else {
            $scope.alert = {header: "Error", message: "Validation error."};
            $scope.hasError = true;
        }
        $scope.showAlert = true;
    };

    $scope.getAlertStyle = function () {
        return $scope.hasError ? "alert alert-danger" : "alert alert-success";
    }

    function clearFields() {
        $scope.newTask.name = "";
        $scope.newTask.description = "";
        $scope.newTask.done = false;
    }
});