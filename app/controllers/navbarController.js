todoListApp.controller('NavbarController', function ($scope, $location, $window) {
    console.log("SaveController creating..");
    $scope.navbar = [];
    $scope.baseUrl = new $window.URL($location.absUrl()).origin + '/#!';

    initData();

    $scope.isActiveUrl = function (path) {
        if($location.path().substr(0, path.length) == path) {
            return true;
        }
        return false;
    };

    function initData() {
        $scope.navbar = [
            {name: "All Tasks", url: "/tasks"},
            {name: "Add Task", url: "/add"}
        ];
    }
});