todoListApp.controller('PreviewTaskController', function ($scope, $routeParams, todoListFactory) {
    console.log("PreviewTaskController creating..");
    $scope.task = {};
    initData();
    function initData() {
        var taskId = $routeParams.taskId;
        console.log('Task id', taskId);
        $scope.task = todoListFactory.getTask(taskId);
    }
});