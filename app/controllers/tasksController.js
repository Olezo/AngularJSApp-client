todoListApp.controller('TasksController', function ($scope, todoListFactory) {
    console.log("TasksController creating..");
    $scope.tasks = [];
    $scope.editId = -1;

    $scope.search = {
        searchOptions: [],
        word: "",
        type: -1
    };

    $scope.showError = false;

    $scope.sorting = {
        sortColumn: "date",
        sortDirection: false
    };

    initData();

    $scope.editTask = function (id) {
        $scope.editId = id;
    };
    $scope.isEditable = function (id) {
        return $scope.editId == id;
    };
    $scope.removeTask = function (id) {
        console.log("Removing task..");
        todoListFactory.removeTask(id);
    };
    $scope.filterTasks = function (searchType) {
        return function (task) {
            if (searchType == 1) {
                return !task.done;
            } else if (searchType == 2) {
                return task.done;
            }
            return true;
        };
    };
    $scope.sortColumns = function (columnName) {
        if ($scope.sorting.sortColumn == columnName) {
            $scope.sorting.sortDirection = !$scope.sorting.sortDirection;
        } else {
            $scope.sorting.sortColumn = columnName;
            $scope.sorting.sortDirection = false;
        }
    };
    $scope.synchronize = function () {
        console.log("Synchronizing...");
        todoListFactory.uploadData();
    };
    
    function initData() {
        console.log("Initialization..");
        $scope.tasks = todoListFactory.getTasks();
        $scope.search.searchOptions = [
            {name: "All", value: -1},
            {name: "Available", value: 1},
            {name: "Done", value: 2}
        ];
        console.log("Initialization finished");
    }
})
;